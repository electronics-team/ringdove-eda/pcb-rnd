#!/bin/sh

RETVAL=0
TESTER=../tester
extra=-s

test_file()
{
	local fn="$1"
	local bn=${fn%%.grbs}
	$TESTER -f $fn -d $extra >$bn.log 2>&1
	if test "$?" = 0
	then
		diff -u $bn.ref $bn.dump && rm $bn.dump $bn.log
		if test "$?" != 0
		then
			RETVAL=1
		fi
	else
		echo "Test exec error in $fn"
		RETVAL=1
	fi
}

if test $# -gt 0
then
# test files specified on CLI
	for n in "$@"
	do
		test_file $n
	done
else
# test all
	for n in *.grbs
	do
		test_file $n
	done
fi

exit $RETVAL
