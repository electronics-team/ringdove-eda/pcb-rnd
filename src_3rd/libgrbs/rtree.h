#ifndef GRBS_RTREE_H
#define GRBS_RTREE_H

#define RTR(n)  grbs_rtree_ ## n
#define RTRU(n) GRBS_RTREE_ ## n
typedef long int grbs_rtree_cardinal_t;
typedef double grbs_rtree_coord_t;
#define grbs_rtree_privfunc static
#define grbs_rtree_size 6
#define grbs_rtree_stack_max 1024

#include <genrtree/genrtree_api.h>

#endif
