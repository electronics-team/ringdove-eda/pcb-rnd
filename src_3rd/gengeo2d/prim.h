#ifndef G2D_PRIM_H
#define G2D_PRIM_H

/* Drawing primitives. This has a separate header so application headers
   that need only the structs won't need to include the inlines too and
   compile them over and over. */

#include <opc89.h>
#include <gengeo2d/common.h>

/*** basic types ***/

typedef funcops struct g2d_vect_s {
	g2d_coord_t x, y;
} g2d_vect_t;

typedef funcops struct g2d_cvect_s {
	g2d_calc_t x, y;
} g2d_cvect_t;

typedef struct g2d_box_s {
	g2d_vect_t p1, p2; /* upper left and lower right points */
} g2d_box_t;

typedef enum g2d_cap_e {
	G2D_CAP_ROUND,   /* width is diameter */
	G2D_CAP_SQUARE   /* width is side length of the square */
} g2d_cap_t;

typedef struct g2d_stroke_s {
	g2d_coord_t width;
	g2d_cap_t cap;
} g2d_stroke_t;

typedef struct g2d_xform_s {
	g2d_calc_t v[9];
} g2d_xform_t; /* 2D transformation matrix */

#define G2D_XFORM_IDENT {{1,0,0,   0,1,0,   0,0,1}}

/*** centerline objects ***/

typedef struct g2d_cline_s { /* compatible with g2d_sline_t */
	g2d_vect_t p1, p2; /* endpoints */
} g2d_cline_t;

typedef struct g2d_carc_s { /* compatible with g2d_sarc_t */
	g2d_vect_t c; /* center point */
	g2d_coord_t r; /* radius */
	g2d_angle_t start, delta;
} g2d_carc_t;

/*** stroked objects ***/

typedef struct g2d_sline_s {
	g2d_cline_t c;
	g2d_stroke_t s;
} g2d_sline_t;

typedef struct g2d_sarc_s {
	g2d_carc_t c;
	g2d_stroke_t s;
} g2d_sarc_t;


/*** special, calculation-typed centerline objects for internal use ***/
typedef struct g2d__cline_s {
	g2d_cvect_t p1, p2; /* endpoints */
} g2d__cline_t;


#endif
