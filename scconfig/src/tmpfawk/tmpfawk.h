/* Load, run and unload an fawk script from input, setting stdout to output.
   Both input and output are paths relative to the CWD of scconfig. wdir
   matters only for redir() from within the script. */
int tmpfawk(const char *wdir, const char *input, const char *output);

/* Same as tmpfawk(), but input and output are relative to wdir. If wdir is NULL,
   wdir is assumed to be the CWD of scconfig (and then the call is equivalent
   to tmpfawk()). */
int tmpfawk_rel(const char *wdir, const char *input, const char *output);

/* Load and run a script, but never unload it. Useful for scripts registering
   new detections. */
int tmpfawk_persistent(const char *script_path);

